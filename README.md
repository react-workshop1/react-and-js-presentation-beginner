# Pokretanje projekta

## React
Za pokretanje potrebno je u konzoli izvr�iti
 - `yarn` - lokalna instalacija paketa
 - `yarn dev` - pokretanje vite projekta

## Bakcend
Radi jednostavnosti *nije izgra�en* cijeli backend nego je pomo�u Wiremocka napravljeno par
stubova koji omogu�uju normalno kori�tenje UI-a

Za pokretanje je potrebno imati instaliran *Docker* i u konzoli izvr�iti `docker compose up -d`

# Prezentacija
Prezentacija se nalazi u `presentation` direktoriju
