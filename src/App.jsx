import './App.css'
import StudentForm from "./components/StudentForm.jsx";
import {createBrowserRouter, Navigate, Outlet, RouterProvider} from "react-router-dom";
import StudentList from "./components/StudentList.jsx";
import Header from "./components/Header.jsx";
import Login from "./components/Login.jsx";
import {useState} from "react";

export default function App() {
  const [loggedIn, setLoggedIn] = useState(false);

  const onLogout = () => {
    setLoggedIn(false);
  }

  if (!loggedIn) {
    return <Login onLogin={() => setLoggedIn(true)}/>;
  }

  const router = createBrowserRouter([
    {
      path: "/",
      element: <AppContainer onLogout={onLogout}/>,
      children: [
        {
          index: true,
          element: <Navigate to="/students"/>
        },
        {
          path: "students",
          element: <StudentList/>
        },
        {
          path: "students/new",
          element: <StudentForm/>
        },
      ]
    }
  ]);

  return (
    <RouterProvider router={router}/>
  );
}

function AppContainer(props) {
  return (
    <div>
      <Header onLogout={props.onLogout}/>
      <Outlet/>
    </div>
  );
}
