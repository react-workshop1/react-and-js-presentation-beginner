import "./Card.css"

export default function Card(props) {
  const {children, title} = props;

  return (
    <div className="Card">
      {title && <h2>{title}</h2>}
      {children}
    </div>
  )
}
