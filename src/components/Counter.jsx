import {useEffect, useState} from "react";

export default function Counter() {
  const [counter, setCounter] = useState(0);

  useEffect(() => {
    document.title = `Counter clicked ${counter} times`
  }, [counter]);

  return (
    <div>
      <span>{counter}</span>
      <button onClick={() => setCounter(0)}>Reset</button>
      <button onClick={() => setCounter(oldCounter => oldCounter + 1)}>
        Increment
      </button>
    </div>
  )
}
