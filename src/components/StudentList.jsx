import Student from "./Student.jsx";
import {useEffect, useState} from "react";
import Card from "./Card.jsx";

export default function StudentList() {
  const [students, setStudents] = useState([]);

  useEffect(() => {
    fetch("/api/students")
      .then(response => response.json())
      .then(data => setStudents(data))
  }, [])

  return (
    <Card title="Students">
      {students.map(student => (
        <Student student={student} key={student.jmbag}/>
      ))}
    </Card>
  );
}
