import {useState} from "react";
import Card from "./Card.jsx";

export default function Login(props) {
  const [form, setForm] = useState({username: "", password: ""});
  const [error, setError] = useState("");

  const onChange = (event) => {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  function isValid() {
    const {username, password} = form;
    return username.length > 0 && password.length > 0;
  }

  const onSubmit = (event) => {
    event.preventDefault();

    const data = JSON.stringify(form);

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json\n"
      },
      body: data,
    }

    fetch("/api/login", options)
      .then(response => {
        if (response.status === 401) {
          setError("Login failed")
        } else {
          props.onLogin();
        }
      });
  }

  return (
    <Card>
      <form onSubmit={onSubmit}>
        <div className="FormRow">
          <label>Username</label>
          <input name="username" onChange={onChange} value={form.username}/>
        </div>
        <div className="FormRow">
          <label>Password</label>
          <input name="password" type="password" onChange={onChange} value={form.password}/>
        </div>
        <div className="error">{error}</div>
        <button disabled={!isValid()}>Login</button>
      </form>
    </Card>
  )
}
