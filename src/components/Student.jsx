function Student(props) {
  const {jmbag, firstName, lastName} = props.student;

  return (
    <p>{firstName} {lastName} ({jmbag})</p>
  )
}

export default Student;
