import {useState} from "react";
import {useNavigate} from "react-router-dom";
import Card from "./Card.jsx";

export default function StudentForm() {
  const [form, setForm] = useState({firstName: "", lastName: "", jmbag: ""});
  const navigate = useNavigate();

  const onChange = (event) => {
    const {name, value} = event.target;
    setForm(oldForm => ({...oldForm, [name]: value}))
  }

  const onSubmit = (event) => {
    event.preventDefault();

    const data = JSON.stringify(form);

    const options = {
      method: "POST",
      headers: {
        "Content-Type": "application/json\n"
      },
      body: data,
    }

    fetch("/api/students", options)
      .then(response => {
        if (response.ok) {
          navigate("/students")
        }
      });
  }

  function isValid() {
    const {firstName, lastName, jmbag} = form;
    return firstName.length > 0 && lastName.length > 0 && jmbag.length === 10
  }

  return (
    <Card title="New student">
      <form onSubmit={onSubmit}>
        <div className="FormRow">
          <label>First name</label>
          <input name="firstName" onChange={onChange} value={form.firstName}/>
        </div>
        <div className="FormRow">
          <label>Last name</label>
          <input name="lastName" onChange={onChange} value={form.lastName}/>
        </div>
        <div className="FormRow">
          <label>JMBAG</label>
          <input name="jmbag" onChange={onChange} value={form.jmbag}/>
        </div>
        <button disabled={!isValid()}>Add student</button>
      </form>
    </Card>
  )
}
