import {Link} from "react-router-dom";
import "./Header.css"

export default function Header(props) {
  const onLogout = () => {
    fetch("/api/logout")
      .then(() => props.onLogout())
  }

  return (
    <header className="Header">
      <Link to="/students">Students</Link>
      <Link to="/students/new">Add students</Link>
      <button onClick={onLogout}>Logout</button>
    </header>
  )
}
